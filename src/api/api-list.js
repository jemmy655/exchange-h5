export default {
    //  83端口接口
    common: {
        getCommonParam: '/api/common/getCommonParam', //GET 获取交易所通用参数--by ob
        getErrorCode: '/api/common/getErrorCode', //GET 获取系统错误码列表枚举--by ob
        getLanguage: '/api/common/getLanguage', //GET 获取当前国际化语言--by ob
        getLegalCurrencyPrices: '/api/common/getLegalCurrencyPrices', //GET 获取计价币种对法币价格
        getTimestamp: '/api/common/getTimestamp', //GET 获取服务器当前时间戳(毫秒)--by ob
        getTimestampJson: '/api/common/getTimestampJson', //GET 获取服务器当前时间戳(毫秒)--by ob
        getUnixTime: '/api/common/getUnixTime', //GET 获取服务器当前时间戳(秒)--by ob
        getUnixTimeJson: '/api/common/getUnixTimeJson', //GET 获取服务器当前时间戳(秒)--by ob
        applyCation:'/api/token/applycation' //POST上币申请接口
    },
    accountCoin: {
        getBalance: '/api/accountCoin/getBalance' //POST 获取账户钱包币种数据--by ob
    },
    account: {
        favoriteSymbol: '/api/account/favoriteSymbol', //POST 交易对收藏操作处理--by ob
        getAccountAgreement: '/api/account/getAccountAgreement', //POST 获取账号的协议同意状态--by ob
        getFavoriteSymbolList: '/api/account/getFavoriteSymbolList', //POST 获取所有收藏的交易对列表--by ob
        getWatchCoinList: '/api/account/getWatchCoinList', //POST 获取所有关注的交易对列表--by ob
        saveAgreementStatus: '/api/account/saveAgreementStatus', //POST 保存账号的协议同意状态--by ob
        watchCoin: '/api/account/watchCoin' //POST 币种关注操作处理--by ob
    },
    verify: {
        checkLoginStatus: '/api/account/checkLoginStatus', // POST 轮循验证是否登录成功--by ob
        checkTransferStatus: '/api/account/checkTransferStatus', // POST 轮循验证是否转账成功--by ob
        iosSimpleWalletVerify: '/api/account/iosSimpleWalletVerify', // POST 针对IOS SDK调用的签名验证--by ob
        tokenPocketVerify: '/api/account/tokenPocketVerify', // POST 验证账号登录授权验证(tokenpocket)--by ob
        transferCallback: '/api/account/transferCallback', // GET 钱包转账回调接口(必带参数type和uuid,...)--by ob
        verify: '/api/account/verify', // POST 验证账号授权状态--by ob
        walletVerify: '/api/account/walletVerify', // POST 钱包扫码验证通用接口--by ob
    },
    coinInfo: {
        getByCode: '/api/coinInfo/getByCode', //POST 根据合约和币种Code获取币种信息--by ob
        getById: '/api/coinInfo/getById', //POST 根据币种ID获取币种信息--by ob
        getBySymbolId: '/api/coinInfo/getBySymbolId', //POST 根据交易对id获取币种信息--by ob
        list: '/api/coinInfo/list', //GET 获取币种列表--by ob
    },
    accountOrder: {
        cancel: '/api/order/cancel', //POST 撤销订单(需要授权验证)--by ob
        cancelAll: '/api/order/cancelAll', //POST 全部撤单(需要授权验证)
        getDealDetailById: '/api/order/getDealDetailById', //POST 获取成交记录详情
        getTrxVolume24H: '/api/order/getTrxVolume24H', //GET 获取24小时成交量
        getHistoryUnReadCount: '/api/order/getHistoryUnReadCount', //POST 查询账号历史委托订单未读订单记录数
        getLastOrderDealRecords: '/api/order/getLastOrderDealRecords', //POST 查询最近交易记录
        getOrderDealRecords: '/api/order/getOrderDealRecords', //POST 查询订单成交记录
        getProcessingUnReadCount: '/api/order/getProcessingUnReadCount', //POST 查询账号当前委托订单未读订单记录数
        getUnReadCount: '/api/order/getUnReadCount', //POST 查询账号未读订单记录数
        list: '/api/order/list', //POST 账户订单列表
        queryCurrentOrderPage: '/api/order/queryCurrentOrderPage', //POST 查询账户当前委托订单信息
        queryHistoryOrderPage: '/api/order/queryHistoryOrderPage', //POST 查询账户历史委托订单信息
        updateHistoryOrderIsReaded: '/api/order/updateHistoryOrderIsReaded', //POST 更新历史委托订单为已读状态
        updateOrderIsReaded: '/api/order/updateOrderIsReaded', //POST 更新订单为已读状态
        updateProcessingOrderIsReaded: '/api/order/updateProcessingOrderIsReaded', //POST 更新当前委托订单为已读状态
        createOrder: '/api/order/createOrder', //POST 创建订单
    },
    symbol: {
        getSymbolInfo: '/api/symbol/getSymbolInfo', //POST 通过交易对id查询交易对信息--by ob
        getSymbolStatus: '/api/symbol/getSymbolStatus', //POST 通过交易对id查询交易对状态信息[状态(0-上架，1-暂停，2-下架中，3-下架，4-待上架)]--by ob
        list: '/api/symbol/list', //POST 获取所有交易对信息(symbol,logo)
        listSymbolByCoinId: '/api/symbol/listSymbolByCoinId', //POST 通过币种id查询相关交易对列表
    },
    version: {
        android: '/api/version/android', //GET 获取android钱包最新版本--by ob
        ios: '/api/version/ios', //GET 获取ios钱包最新版本--by ob
        web: '/api/version/web', //GET Web钱包版本要求接口--by ob
    },
    accountToken: {
        checkTokenStatus: '/api/token/checkTokenStatus', //检查token的状态信息--by ob
    },
    //  82端口接口
    // 订单处理接口
    deal: {
        cancelAll: '/deal/order/cancelAll', // POST 全部撤单（前端调用）
        cancelOrder: '/deal/order/cancelOrder', // POST 订单撤单（前端调用）
        cancelOrderBackstage: '/deal/order/cancelOrderBackstage', // POST 订单撤单（后台处理）
        cancelOrderNoTransfer: '/deal/order/cancelOrderNoTransfer', // POST 撤单不转账（后台处理）
        restoreOrder: '/deal/order/restoreOrder', // POST 恢复订单（后台处理）
    }
}