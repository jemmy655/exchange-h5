const path = require('path');

function resolve(dir) {
	return path.join(__dirname, dir);
}

console.log('本地启动或构建的文件信息 | ------------------------------开始--------------------------------');

console.log('本地启动或构建的文件信息 | ------------------------------结束--------------------------------');


// const baseUrl = process.env.NODE_ENV === 'production' ? '/multi-page-Vue/dist/' : '/';

module.exports = {
	// 项目在服务器的根目录
	baseUrl: '/',
	// 打包目录
	// outputDir: 'dist',
	// 打包后静态资源存放目录，默认为"static"
	assetsDir: 'static',
	// lintOnSave: true,
	// runtimeCompiler: false,
	productionSourceMap: false,
	// parallel: undefined,
	// css: {
	//   extract: true,
	//   sourceMap: false,
	// },

	devServer: {
		proxy: {
			'/api': {
				// 本地项目地址最好统一路径 要不在dev时自行修改
				target: 'http://192.168.0.15:8083', //内网
				// target: 'http://120.41.186.187:8083', //外网
				// target: 'https://front.Kiwidex.io', //外网
				ws: true,
				changeOrigin: true
			},
			'/deal': {
				target: 'http://192.168.0.15:8082', //内网
				// target: 'http://120.41.186.187:8082', //外网
				// target: 'https://deal.Kiwidex.io', //正式
				ws: true,
				changeOrigin: true
			}
		},
	},
	// configureWebpack: (config) => {
	//   vuxLoader.merge(config, {
	//     plugins: [
	//       'vux-ui',
	//       {
	//         name: 'duplicate-style',
	//         options: {
	//           cssProcessorOptions: {
	//             safe: true,
	//             zindex: false,
	//             autoprefixer: {
	//               add: true,
	//               browsers: [
	//                 'iOS >= 7',
	//                 'Android >= 4.1',
	//               ],
	//             },
	//           },
	//         },
	//       },
	//       {
	//         name: 'less-theme',
	//         path: 'src/common/styles/theme.less', // 相对项目根目录路径
	//       },
	//     ],
	//   });
	//   if (process.env.NODE_ENV === 'production') {
	//     // 为生产环境修改配置...
	//   } else {
	//     // 为开发环境修改配置...
	//   }
	// },

	chainWebpack: (config) => {
	  config.resolve.alias
	    .set('@', resolve('src'))
	    .set('_assets', resolve('src/assets'));
	},
	// devServer: {
	//   proxy: {
	//     '/api': {
	//       target: 'https://newdex.340wan.com',
	//       ws: true,
	//       changeOrigin: true,
	//     },
	//   },
	// },

	// lintOnSave: false,
};